# Example Sofa Configurator

This repository contains a simplified version of Fixtuur's Sofa Configurator code found in Visual Space. For the purposes of interviewing software developers and talking through code examples which are similar to our real codebase without allowing other concerns, the realities of production code, and other incidental complexities pollute the code.
