/**
 * The Configurator will build a Sofa of a particular Shape
 *
 * Sofas are made up of Seats, where each Seat
 * has a Kind, a Position, and an Orientation.
 *
 * Sofas are constructed in 2-dimensions looking top-down
 *
 * We assume that each Seat is sized a single unit in
 * the x and y directions for simplicity. Each seat is
 * assumed to fit a single person.
 *
 * This has all been brought into a single file for simplicity.
 *
 * Sections, denoted by comments starting with 'Section:', are used
 * in place of files
 */

// Section: Maths

// even though they're defined externally
type Position = { x: number; y: number };
// rotation around the Y axis
type Orientation = number;
type Vector2 = [number, number];

// can't call origin - already a const by that name
const originPosition: Position = { x: 0, y: 0 };
const identityOrientation: Orientation = 0;

const translate = (p: Position, v: Vector2): Position => ({
  x: p.x + v[0],
  y: p.y + v[1],
});

// calculate local x + z vectors in the coordinate space of a given Orientation
const basis = (o: Orientation): { localX: Vector2; localZ: Vector2 } => {
  const localX: Vector2 = [Math.cos(o), Math.sin(o)];
  return {
    localX: localX,
    localZ: [-localX[1], localX[0]],
  };
};

const scale = (v: Vector2, scalar: number): Vector2 =>
  v.map((el) => el * scalar) as Vector2;

const sum = (l: Vector2, r: Vector2): Vector2 => [l[0] + r[0], l[1] + r[1]];

// Section: Seat

/** A Sofa is made up of Seats.
 * Seats can be of any given SeatKind */
type SeatKind = "middle" | "corner" | "end";

type Seat = { kind: SeatKind; position: Position; orientation: Orientation };

// calculates the place of a single seat based on the current position and orientation.
const placeSeat = (
  position: Position,
  orientation: Orientation,
  kind: SeatKind
): {
  seat: Seat;
  currentPosition: Position;
  currentOrientation: Orientation;
} => {
  // seats are always 1 unit wide
  const nextPositionOffset = 1;
  const seatPositionOffset = 1 / 2;

  let placementOffset, placementPosition, currentPosition, currentOrientation;
  const { localX, localZ } = basis(orientation);

  if (kind === "corner") {
    placementOffset = sum(
      scale(localZ, seatPositionOffset),
      // depth is always 1 unit
      scale(localX, 1 / 2)
    );

    placementPosition = translate(position, placementOffset);

    // it's a corner piece, so we have to rotate
    currentOrientation = orientation + Math.PI / 2;

    const cornerPositionOffset = 1;

    currentPosition = translate(
      position,
      sum(
        scale(localX, cornerPositionOffset),
        scale(localZ, nextPositionOffset)
      )
    );
  } else {
    placementOffset = sum(
      scale(localX, seatPositionOffset),
      scale(localZ, 1 / 2)
    );

    placementPosition = translate(position, placementOffset);
    currentPosition = translate(position, scale(localX, nextPositionOffset));
    currentOrientation = orientation;
  }

  return {
    seat: {
      kind,
      position: placementPosition,
      orientation: currentOrientation,
    },
    currentPosition,
    currentOrientation,
  };
};

// Section: Sofa

type Sofa = Seat[];

/** The description of the Sofa */
type SofaConfig =
  | { shape: "straight"; length: number }
  | { shape: "corner"; left: number; right: number };

const emptySofa: Sofa = [];

const placeSeats = (kinds: Array<SeatKind>): Sofa => {
  let currentPosition = originPosition;
  let currentOrientation = identityOrientation;

  const placed: Sofa = emptySofa;

  for (let kind of kinds) {
    const placement = placeSeat(currentPosition, currentOrientation, kind);

    currentPosition = placement.currentPosition;
    currentOrientation = placement.currentOrientation;

    placed.push(placement.seat);
  }

  return placed;
};

/**
 * Sofa Rules:
 * At each end must be an 'end' piece.
 */
const buildSofa = (config: SofaConfig): Sofa => {
  switch (config.shape) {
    case "straight":
      return placeSeats([
        "end",
        ...new Array(config.length).fill("middle"),
        "end",
      ]);
    case "corner":
      return placeSeats([
        "end",
        ...new Array(config.left).fill("middle"),
        "corner",
        ...new Array(config.right).fill("middle"),
        "end",
      ]);
  }
};
